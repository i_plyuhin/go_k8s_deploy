## Здесь первый контейнер с библиотеками голанг будет только строить наш сервер, имя у него будет build
#FROM golang:1.16-alpine AS build
#
## определяем рабочую директорию, теперь все команды будут запускаться отсюда
#WORKDIR /app
#
## копируем файлы зависимостей
#COPY go.mod ./
#COPY go.sum ./
## качаем зависимости
#RUN go mod download
#
## копируем исходники
#COPY *.go ./
#
## собираем сервер
#RUN CGO_ENABLED=0 go build -o /server
#
#
## а здесь мы только копируем уже собранный сервер и запускаем его

# Specific dockerbuild for gitlab CI
FROM gcr.io/distroless/static

ARG ARTIFACT_PATH

WORKDIR /

COPY $ARTIFACT_PATH /server

EXPOSE 8080

USER nonroot:nonroot

ENTRYPOINT ["/server"]
